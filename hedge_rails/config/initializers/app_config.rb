APP_CONFIG = YAML.load_file(Rails.root.join('config/config.yml'))[Rails.env]
SERVICES = YAML.load_file(Rails.root.join('config/services.yml'))[Rails.env]