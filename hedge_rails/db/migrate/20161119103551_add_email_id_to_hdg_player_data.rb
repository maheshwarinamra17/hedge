class AddEmailIdToHdgPlayerData < ActiveRecord::Migration
  def change
    add_column :hdg_player_data, :email_id, :text
  end
end
