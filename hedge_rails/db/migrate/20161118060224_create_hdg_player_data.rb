class CreateHdgPlayerData < ActiveRecord::Migration
  def change
    create_table :hdg_player_data do |t|
      t.text :name
      t.text :city
      t.integer :age
      t.string :portfolio_value
      t.string :gold_value
      t.string :cash_value
      t.datetime :created_at
      t.datetime :updated_at
    end
  end
end
