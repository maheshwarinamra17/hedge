class AddIndexToHdgStockData < ActiveRecord::Migration
  def change
    add_index :hdg_stock_data, :symbol, :unique => true
  end
end
