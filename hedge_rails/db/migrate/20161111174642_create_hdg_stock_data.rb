class CreateHdgStockData < ActiveRecord::Migration
  def change
    create_table :hdg_stock_data do |t|
      t.string :name
      t.string :symbol
      t.string :exchange
      t.integer :volume
      t.float :lastvalue
      t.float :change
      t.float :percentage_change
      t.datetime :source_last_update
      t.float :day_high
      t.float :day_low
      t.integer :color
    end
  end
end
