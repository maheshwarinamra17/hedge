class CreateHdgTransactionData < ActiveRecord::Migration
  def change
    create_table :hdg_transaction_data do |t|
      t.integer :player_id
      t.integer :type
      t.integer :quantity
      t.string :symbol
      t.float :price
      t.datetime :created_at
      t.datetime :updated_at
    end
  end
end
