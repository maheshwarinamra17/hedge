class AddTimestampsToHdgStockData < ActiveRecord::Migration
  def change
    add_column(:hdg_stock_data, :created_at, :datetime)
    add_column(:hdg_stock_data, :updated_at, :datetime)
  end
end
