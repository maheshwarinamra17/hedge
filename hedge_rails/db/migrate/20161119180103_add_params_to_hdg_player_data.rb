class AddParamsToHdgPlayerData < ActiveRecord::Migration
  def change
    add_column :hdg_player_data, :password, :string
    add_column :hdg_player_data, :auth_token, :string
    add_column :hdg_player_data, :buy_transactions, :text
  end
end
