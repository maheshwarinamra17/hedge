class ChangeColumnNameHdgStockData < ActiveRecord::Migration
  def change
    rename_column :hdg_stock_data, :change, :value_change
  end
end
