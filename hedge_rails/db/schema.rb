# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161119180103) do

  create_table "hdg_player_data", force: :cascade do |t|
    t.text     "name",             limit: 65535
    t.text     "city",             limit: 65535
    t.integer  "age",              limit: 4
    t.string   "portfolio_value",  limit: 255
    t.string   "gold_value",       limit: 255
    t.string   "cash_value",       limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "email_id",         limit: 65535
    t.string   "password",         limit: 255
    t.string   "auth_token",       limit: 255
    t.text     "buy_transactions", limit: 65535
  end

  create_table "hdg_stock_data", force: :cascade do |t|
    t.string   "name",               limit: 255
    t.string   "symbol",             limit: 255
    t.string   "exchange",           limit: 255
    t.integer  "volume",             limit: 4
    t.float    "lastvalue",          limit: 24
    t.float    "change",             limit: 24
    t.float    "percentage_change",  limit: 24
    t.datetime "source_last_update"
    t.float    "day_high",           limit: 24
    t.float    "day_low",            limit: 24
    t.integer  "color",              limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "hdg_stock_data", ["symbol"], name: "index_hdg_stock_data_on_symbol", unique: true, using: :btree

  create_table "hdg_transaction_data", force: :cascade do |t|
    t.integer  "player_id",  limit: 4
    t.integer  "type",       limit: 4
    t.integer  "quantity",   limit: 4
    t.string   "symbol",     limit: 255
    t.float    "price",      limit: 24
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
