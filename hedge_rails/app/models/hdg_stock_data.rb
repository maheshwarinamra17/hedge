class HdgStockData < ActiveRecord::Base

  def self.store_color hex_code
    result = 0
    if(hex_code == "#009900")
      result = 1
    elsif(hex_code == "#CC0000")
      result = 2
    end
    return result
  end
  def self.create_stock_entry data
    stock_entry = HdgStockData.new
    stock_entry.name = "STOCK NAME"
    stock_entry.symbol = data['stock_code']
    stock_entry.exchange = data['exchange']
    stock_entry.volume = data['volume']
    stock_entry.value_change = data['change']
    stock_entry.percentage_change = data['perchange']
    stock_entry.source_last_update = data['lastupdate']
    stock_entry.color = store_color(data['color'])
    stock_entry.lastvalue = data['lastvalue']
    stock_entry.day_high = data['dayhigh']
    stock_entry.day_low = data['daylow']
    stock_entry.created_at = Time.now
    stock_entry.updated_at = Time.now
    result = stock_entry.save
    return result
  end

  def self.update_stock_entry data
    updated_entry = HdgStockData.find_by_symbol(data['stock_code'])
    if updated_entry.present?
      updated_entry.update(
          updated_at:Time.now,
          volume: data['volume'],
          value_change: data['change'],
          percentage_change: data['perchange'],
          source_last_update: data['lastupdate'],
          color: store_color(data['color']),
          lastvalue: data['lastvalue'],
          day_high: data['dayhigh'],
          day_low: data['daylow'],
      )
      result = updated_entry.save
    else
      result = false
    end
    return result
  end

  def self.find_stock_entry stock_code, exchange
    stock_entry  = HdgStockData.find_by_symbol(stock_code + "_" + exchange)
    return stock_entry
  end
end