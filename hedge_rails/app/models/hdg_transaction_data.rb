class HdgTransactionData < ActiveRecord::Base

  def self.create_transaction_entry data
    result = {}
    transaction_entry = HdgTransactionData.new
    transaction_entry.quantity = data['qty']
    transaction_entry.type = data['type']
    transaction_entry.symbol = data['symbol']
    transaction_entry.price = data['lastvalue']
    transaction_entry.player_id = data['player_id']
    transaction_entry.created_at = Time.now
    transaction_entry.updated_at = Time.now
    result['data'] = transaction_entry.save
    if(data['type'] == 1)
      transaction_log = HdgPlayerData.update_buy_transaction(transaction_entry.player_id, transaction_entry.price, transaction_entry.quantity,transaction_entry.symbol)
    else
      transaction_log = HdgPlayerData.update_sell_transaction(transaction_entry.player_id, transaction_entry.price, transaction_entry.quantity,transaction_entry.symbol)
    end
    result['data'] = result['data'] && transaction_log['data']
    if(transaction_log['error'].present?)
      result['error'] = transaction_log['error']
    end
    return result
  end


end