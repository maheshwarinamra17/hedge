class HdgPlayerData < ActiveRecord::Base

  def self.unique_authentication_token
    result = nil
    loop do
      result = SecureRandom.urlsafe_base64
      break unless HdgPlayerData.find_by(auth_token: result)
    end
    return result
  end

  def self.destroy_authentication_token token
    player = HdgPlayerData.where(:auth_token => token).first
    result = false
    if player.present?
      result = change_authentication_token player.id, nil
    end
    return result
  end

  def self.create_player_entry data
    result = {}
    player_entry = HdgPlayerData.new
    player_entry.name = data['name']
    player_entry.age = data['age']
    player_entry.password = data['password']
    player_entry.city = data['city']
    player_entry.email_id = data['email_id']
    player_entry.portfolio_value = APP_CONFIG['new_player']['portfolio_value']
    player_entry.cash_value = APP_CONFIG['new_player']['cash_value']
    player_entry.gold_value = APP_CONFIG['new_player']['gold_value']
    player_entry.created_at = Time.now
    player_entry.updated_at = Time.now
    result['new_player'] = player_entry.save
    if(result['new_player'])
      result['player_id'] = player_entry.id
      result['portfolio_value'] = player_entry.portfolio_value
      result['gold_value'] = player_entry.gold_value
      result['cash_value'] = player_entry.cash_value
    end
    return result
  end

  def self.update_buy_transaction player_id, price, quantity, symbol
    result = {}
    updated_entry = HdgPlayerData.find(player_id)
    trans_integrity = PlayerDataHandler.player_transaction_integrity(updated_entry.cash_value, price, quantity, symbol, APP_CONFIG['BUY'], updated_entry.buy_transactions)
    if(trans_integrity['data'])
      buy_transactions = symbol + ":" + quantity.to_s + ":" + price.to_s
      if(updated_entry.buy_transactions.present?)
        buy_transactions =  PlayerDataHandler.create_buy_transaction(updated_entry.buy_transactions, price, quantity, symbol)
      end
      value_adjust = PlayerDataHandler.value_adjustments_buy(updated_entry.portfolio_value.to_f, updated_entry.cash_value.to_f, price, quantity)
      if updated_entry.present? && !value_adjust['error'].present?
        updated_entry.update(
            updated_at: Time.now,
            buy_transactions: buy_transactions,
            portfolio_value: value_adjust['portfolio_value'].round(2),
            cash_value: value_adjust['cash_value'].round(2)
        )
        result['data'] = updated_entry.save
      else
        result['data'] = false
      end
    else
      result = trans_integrity
    end
    return result
  end

  def self.update_sell_transaction player_id, price, quantity, symbol
    result = {}
    updated_entry = HdgPlayerData.find(player_id)
    if(updated_entry.buy_transactions.present?)
      trans_integrity = PlayerDataHandler.player_transaction_integrity(updated_entry.cash_value, price, quantity, symbol, APP_CONFIG['SELL'], updated_entry.buy_transactions)
    end
    if (trans_integrity['data'])
      sell_transaction =  PlayerDataHandler.create_sell_transaction(updated_entry.buy_transactions, quantity, symbol)
      value_adjust = PlayerDataHandler.value_adjustments_sell(updated_entry.portfolio_value.to_f, updated_entry.cash_value.to_f, price, quantity)
      if updated_entry.present? && !value_adjust['error'].present?
        updated_entry.update(
            updated_at: Time.now,
            buy_transactions: sell_transaction,
            portfolio_value: value_adjust['portfolio_value'].round(2),
            cash_value: value_adjust['cash_value'].round(2)
        )
        result['data'] = updated_entry.save
      else
        result['data'] = false
      end
    else
      result = trans_integrity
    end
    return result
  end

  def self.change_authentication_token user_id, token
    updated_token = HdgPlayerData.find(user_id)
    updated_token.update(
        auth_token:token
    )
    return updated_token.save
  end

  def self.create_player_session email_id, password
    player = HdgPlayerData.where(:email_id => email_id, :password => password).select(:id,:name, :email_id, :city, :age, :portfolio_value, :cash_value, :gold_value).first
    if player.present?
      token = unique_authentication_token
      token_flag = change_authentication_token player.id, token
      if(token_flag)
        player['auth_token'] = token
      end
    end
    return player
  end

end