class CommunicationMailer < ApplicationMailer

  def signup_email email_id
    mail(to: email_id, subject: 'Verifying Account: Welcome to Hedge', body: 'Test Email')
  end

end
