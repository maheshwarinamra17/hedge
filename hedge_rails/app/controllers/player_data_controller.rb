class PlayerDataController < ApplicationController

  def create_new_player
    ## Check for city and email corrections
    name = params[:name]
    email_id = params[:email_id]
    city = params[:city]
    age =  params[:age]
    password =  params[:password]
    result = PlayerDataHandler.create_new_player(name, email_id, city, age, password)
    SignupMailJob.new(email_id).enqueue(wait: 10.seconds)
    render :json => result and return
  end

  def player_login
    username = params[:username]
    password = params[:password]
    result = {}
    if !username.present? and !password.present?
      result['data'] = nil
      result['error'] = "Incorrect parameters: username, password"
    else
      player = HdgPlayerData.create_player_session(username, password)
      if !player.present?
        result['data'] = nil
        result['error'] = "Incorrect parameters: username, password"
      else
        result['data'] = player
      end
    end
    render :json => result and return
  end

  def player_logout
    result = {}
    token = nil
    authenticate_with_http_token do |t|
      token = t
    end
    result['data'] = false
    if !token.present?
      result['error'] = "Incorrect parameters: token"
    else
      result['data'] = HdgPlayerData.destroy_authentication_token(token)
      if(!result['data'])
        result['error'] = "Logout status: failed"
      end
    end
    render :json => result and return
  end

end
