class StockDataController < ApplicationController

  # before_action :authenticate

  def get_stock_list
    result = nil
    if APP_CONFIG['stocks'].present?
      result = APP_CONFIG['stocks']
    end
    render :json =>result and return
  end

  def get_stock_data
    stock_code = params[:code]
    exchange = params[:ex]
    result = StockDataHandler.fetch_stock_data_domestic(stock_code,exchange)
    render :json => result and return
  end

  def create_stock_data
    result = nil
    stock_code = params[:code]
    if(stock_code.present?)
      stock_data = StockDataHandler.get_stock_data_domestic(stock_code)
      result =  StockDataHandler.create_new_stock_domestic(stock_data)
     end
    render :json =>{"data" => result} and return
  end

end
