class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  # protect_from_forgery with: :exception
  # use CSRF token security along with our auth token security
  protect_from_forgery

  protected

  def authenticate
    authenticate_token || render_unauthorized
  end

  def authenticate_token
    authenticate_with_http_token do |token|
      player =  HdgPlayerData.find_by_auth_token(token)
      if player.present?
        @player_id = player.id
        return true
      else
        return false
      end
    end
  end

  def render_unauthorized
    result = {}
    self.headers['WWW-Authenticate'] = 'Token realm="Hedge"'
    result['data'] = nil
    result['error'] = "Unauthorized player: invalid token"
    render :json => result and return
  end

end
