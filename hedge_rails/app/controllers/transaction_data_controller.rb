class TransactionDataController < ApplicationController

  def buy_a_stock
    stock_code = params[:code]
    exchange = params[:ex]
    quantity = params[:qty]
    player_id = 5
    result = TransactionDataHandler.buy_a_stock(stock_code,exchange,quantity,player_id)
    render :json => result and return
  end

  def sell_a_stock
    stock_code = params[:code]
    exchange = params[:ex]
    quantity = params[:qty]
    player_id = 5
    result = TransactionDataHandler.sell_a_stock(stock_code,exchange,quantity,player_id)
    render :json => result and return
  end

end
