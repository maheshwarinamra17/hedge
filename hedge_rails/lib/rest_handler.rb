# http://johnnunemaker.com/httparty/

class RestHandler

  def initialize object
    @url =  object['url']
    @query = object['query']
    @content_type = object["content_type"]
    @body  = object["body"]
  end

  def send_get_request
    request = @url + @query
    response = HTTParty.get(request)
    return response
  end


end