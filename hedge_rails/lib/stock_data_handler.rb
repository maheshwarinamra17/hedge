class StockDataHandler

  def self.is_valid_stock stock_code
    if stock_code.present? && APP_CONFIG['stocks'][stock_code].present?
      return true
    else
      return false
    end
  end

  def self.get_stock_data_domestic stock_code
    if !is_valid_stock(stock_code)
      return nil
    end
    request_object = {
        "url" => SERVICES['domestic_stock_data'],
        "query" => "?a=" + APP_CONFIG['stocks'][stock_code]['key'] + "&callback=stk_det"
    }
    rest_handler =  RestHandler.new(request_object)
    request =  rest_handler.send_get_request
    result = clean_stock_data_domestic(request)
    result['stock_code'] = stock_code
    return result
  end

  def self.clean_stock_data_domestic raw_object
    json_string =  raw_object.tr('()','')[7..-1]
    result = {}
    begin
      result = JSON.parse(json_string)
    rescue
      result = nil
    end
    return result
  end

  def self.create_new_stock_domestic data
    result = {}
    if(data['bse'].length > 1)
      stock_data = data['bse']
      stock_data['exchange'] = "bse"
      stock_data['stock_code'] = data['stock_code']+"_"+stock_data['exchange']
      result['bse'] = HdgStockData.create_stock_entry(stock_data)
    else
      result['bse'] =  false
    end

    if(data['nse'].length > 1)
      stock_data = data['nse']
      stock_data['exchange'] = "nse"
      stock_data['stock_code'] = data['stock_code']+"_"+stock_data['exchange']
      result['nse'] = HdgStockData.create_stock_entry(stock_data)
    else
      result['nse'] =  false
    end
    return result
  end

  def self.update_stock_domestic data
    result = {}
    if(data['bse'].length > 1)
      stock_data = data['bse']
      stock_data['exchange'] = "bse"
      stock_data['stock_code'] = data['stock_code']+"_"+stock_data['exchange']
      result['bse'] = HdgStockData.update_stock_entry(stock_data)
    else
      result['bse'] =  false
    end

    if(data['nse'].length > 1)
      stock_data = data['nse']
      stock_data['exchange'] = "nse"
      stock_data['stock_code'] = data['stock_code']+"_"+stock_data['exchange']
      result['nse'] = HdgStockData.update_stock_entry(stock_data)
    else
      result['nse'] =  false
    end
    return result
  end

  def self.fetch_stock_data_domestic stock_code, exchange
    result = {}
    if(stock_code.present? && exchange.present? && is_valid_stock(stock_code))
      db_stock_data = HdgStockData.find_stock_entry(stock_code,exchange)
      result['data'] = db_stock_data
    else
      result['data'] = nil
      result['error'] = "Incorrect parameters: stock_code, exchange"
    end
    return result
  end

end