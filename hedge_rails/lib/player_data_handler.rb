class PlayerDataHandler

  def self.player_params_integrity name, email_id, city, age, password
    ## Email should be formattted and not taken
    ## Age should be between 10-99
    result = {}
    result['valid'] = true
    result['error'] = "Nahi Pata abhi"
    return result
  end

  def self.player_transaction_integrity old_cash_value, price, quantity, symbol, type, buy_transactions
    result = {}
    result['data'] = true
    if (quantity < 1)
      result['error'] = "Incorrect quantity: " + quantity.to_s
      result['data'] = false
      return result
    end
    if(type == 1)
     if (old_cash_value.to_f < price * quantity)
        result['error'] = "Insufficient cash for quantity: "+ quantity.to_s
        result['data'] = false
      end
    else
      trans_hash = decompress_buy_transactions buy_transactions
      if(!trans_hash.key?(symbol))
        result['error'] = "Stock code not found: "+ symbol
        result['data'] = false
      else
        if(trans_hash[symbol]['qty'] < quantity)
          result['error'] = "Insufficient quantity: allowed quantity is "+ trans_hash[symbol]['qty'].to_s
          result['data'] = false
        end
      end
    end
    return result
  end

  def self.value_adjustments_buy old_portfolio_value, old_cash_value, price, quantity
    result = {}
    multiplier = price * quantity
    factor = (1 + APP_CONFIG['brokerage']) * multiplier
    if(factor < old_cash_value)
      result['portfolio_value'] = old_portfolio_value + multiplier
      result['cash_value'] = old_cash_value - factor
    else
      result['error'] = "Incorrect transaction: insufficient cash"
    end
    return result
  end

  def self.value_adjustments_sell old_portfolio_value, old_cash_value, price, quantity
    result = {}
    multiplier = price * quantity
    factor = (1 - APP_CONFIG['brokerage']) * multiplier
    if(factor < old_portfolio_value)
      result['portfolio_value'] = old_portfolio_value - multiplier
      result['cash_value'] = old_cash_value + factor
    else
      result['error'] = "Incorrect transaction: insufficient portfolio value"
    end
    return result
  end

  def self.create_sell_transaction buy_transactions, quantity, symbol
    trans_hash = decompress_buy_transactions(buy_transactions)
    if(trans_hash[symbol].present? && trans_hash[symbol]['qty'] >= quantity)
      if(trans_hash[symbol]['qty'] == quantity)
        trans_hash.delete(symbol)
      else
        trans_hash[symbol]['qty'] =  trans_hash[symbol]['qty'] - quantity
      end
    end
    result = compress_buy_transactions trans_hash
    return result
  end

  def self.create_buy_transaction buy_transactions, price, quantity, symbol
    trans_hash = decompress_buy_transactions(buy_transactions)
    if(trans_hash[symbol].present?)
      trans_hash[symbol]['price'] = ((trans_hash[symbol]['price'] * trans_hash[symbol]['qty']) + (price * quantity))/(trans_hash[symbol]['qty'] + quantity)
      trans_hash[symbol]['qty'] =  trans_hash[symbol]['qty'] + quantity
    else
      trans_hash[symbol] = {}
      trans_hash[symbol]['price'] =  price
      trans_hash[symbol]['qty'] = quantity
    end
    result = compress_buy_transactions trans_hash
    return result
  end

  def self.compress_buy_transactions trans_hash
    result = ""
    trans_hash.each do |key,value|
      result =  result + key + ":" + value['qty'].to_s + ":" + value['price'].to_s + ";"
    end
    return result
  end

  def self.decompress_buy_transactions buy_transactions
    result = {}
    trans_array = buy_transactions.split(';')
    trans_array.each do |trans|
      trans = trans.split(':')
      result[trans[0]] = {"qty" => trans[1].to_i, "price" =>(trans[2].to_f).round(2)}
    end
    return result
  end

  def self.create_new_player name, email_id, city, age, password
    result = {}
    data = {}
    result['data'] =  nil
    player = player_params_integrity  name, email_id, city, age, password
    if(!name.present? || !email_id.present? || !city.present? || !age.present? || !password.present?)
      result['error'] = "Incorrect parameters: name, email_id, city, age, password"
    elsif !player['valid']
      result['error'] = player['error']
    else
      data['name'] = name
      data['email_id'] = email_id
      data['city'] = city
      data['age'] = age
      data['password'] = password
      result['data'] = HdgPlayerData.create_player_entry(data)
    end
    return result
  end

end