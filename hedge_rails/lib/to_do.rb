#TODO: ===== RAILS ======
#TODO: [DONE] API: get_player_info  
#TODO: API: get_player_portfolio - Array of {name, symbol, current price, percentage gain/loss to user, number of stocks}
#TODO: API: get_player_wallet - {portfolio_value, cash_in_hand, gold, gain/loss today}
#TODO: [DONE] API: get_stock_list - write all details from config
#TODO:  Add category to config and 300 stocks minimum
#TODO: [DONE] get_stock_data
#TODO: [BIGJOB, PYTHON, RAILS] get_stock_graph 
#TODO: API: get_player_stock_data: Get player stock data if bought 
#TODO: [DONE] API: buy_a_stock
#TODO: [DONE] API: sell_a_stock
#TODO: get_leader_board_overall: {rank, name, portfolio} with pagination  
#TODO: get_leader_board_today: {rank, name, portfolio} with pagination 
#TODO: get_transaction_data: Last 42 or week which ever is less.
#TODO: [API] Login, forgot password etc.
#TODO: [API] Watchlist

#TODO: ===== SKETCH =====
#TODO: Close the design of v1.0