class TransactionDataHandler

  def self.transaction_integrity type, quantity, player_id
    ## Qauntity > 0
    result = {}
    result['valid'] = true
    result['error'] = "Nahi Pata abhi"
    return result
  end

  def self.buy_a_stock stock_code, exchange, quantity, player_id
    result = {}
    data = {}
    result['data'] = nil
    data['type'] = APP_CONFIG['BUY']
    transaction =  transaction_integrity(data['type'], quantity, player_id)
    if(!stock_code.present? || !exchange.present? || !quantity.present? || !player_id.present?)
      result['error'] = "Incorrect parameters: code, ex, qty, player_id"
    elsif !transaction['valid']
      result['error'] = transaction['error']
    else
      data['symbol'] = stock_code + "_" + exchange
      data['qty'] =  quantity
      data['player_id'] = player_id
      stock_data = StockDataHandler.fetch_stock_data_domestic(stock_code,exchange)
      if(stock_data['data'].present? && !stock_data['error'].present?)
        data['lastvalue'] =  stock_data['data']['lastvalue']
        result = HdgTransactionData.create_transaction_entry(data)
      else
        result['error'] = stock_data['error'] || "Stock data not present"
      end
    end
    return result
  end

  def self.sell_a_stock stock_code, exchange, quantity, player_id
    result = {}
    data = {}
    result['data'] = nil
    data['type'] = APP_CONFIG['SELL']
    transaction =  transaction_integrity(data['type'], quantity, player_id)
    if(!stock_code.present? || !exchange.present? || !quantity.present? || !player_id.present?)
      result['error'] = "Incorrect parameters: code, ex, qty, player_id"
    elsif !transaction['valid']
      result['error'] = transaction['error']
    else
      data['symbol'] = stock_code + "_" + exchange
      data['qty'] =  quantity
      data['player_id'] = player_id
      stock_data = StockDataHandler.fetch_stock_data_domestic(stock_code,exchange)
      if(stock_data['data'].present? && !stock_data['error'].present?)
        data['lastvalue'] =  stock_data['data']['lastvalue']
        result = HdgTransactionData.create_transaction_entry(data)
      else
        result['error'] = stock_data['error'] || "Stock data not present"
      end
    end
    return result
  end

end