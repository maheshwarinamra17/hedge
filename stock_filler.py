import urllib2, json, MySQLdb, time
from prettytable import PrettyTable
from datetime import date

config = {
    "db_name": "hedge_production",
    "db_uname": "root",
    "db_passwd": "root",
    "db_table": "hdg_stock_data",
    "domain_url": "http://m.moneycontrol.com/get_stock_price_mem.php",
    "db_logger": "stock_filler.log",
    "exchange": ["nse","bse"],
    "stock_codes": "http://127.0.0.1/get_stock_list"
}

class StockFiller(object):

    def __init__(self):
        global config
        self.db_connect = MySQLdb.connect("localhost", config['db_uname'] ,config['db_passwd'], config['db_name'])
        self.db_cursor = self.db_connect.cursor()
        self.table_name = config['db_table']
        self.domain_url = config['domain_url']

    def clean_stock_data(self,data):
        result = data.replace(')','')
        result = json.loads(result[8:])
        return result

    def color_code(self,hex_code):
        result = "0"
        if(hex_code == "#009900"):
            result = "1"
        elif(hex_code == "#CC0000"):
            result = "2"
        return result

    def convert_timestamp(self, time_stamp):
        result = time.strftime('%Y-%m-%d %H:%M:%S',time.strptime(str(date.today().year)+" "+time_stamp,'%Y %b %d, %H:%M'))
        return result

    def update_query(self,symbol,data, exchange):
        current_time = time.strftime('%Y-%m-%d %H:%M:%S')
        symbol = symbol + "_" + exchange
        volume = str(data['volume'])
        lastvalue = str(data['lastvalue'].replace(',',''))
        value_change = str(data['change'].replace(',',''))
        percentage_change = str(data['perchange'])
        source_last_update = self.convert_timestamp(str(data['lastupdate']))
        day_high = str(data['dayhigh'].replace(',',''))
        day_low = str(data['daylow'].replace(',',''))
        color = self.color_code(data['color'])
        updated_at = current_time
        result = "UPDATE " + self.table_name + " SET volume='" + volume + "', lastvalue='" + lastvalue + "', percentage_change='" + percentage_change + "', source_last_update='" + source_last_update + "', day_high='" + day_high + "', day_low='" + day_low + "', color='" + color + "', updated_at='" + updated_at + "', value_change = '" + value_change + "' WHERE symbol='" + symbol + "';"
        return result

    def insert_query(self,symbol,exchange):
        current_time = time.strftime('%Y-%m-%d %H:%M:%S')
        stock_codes =  self.load_stock_codes()
        name =  stock_codes[symbol]['name']
        result = "INSERT INTO " + self.table_name + " (symbol, exchange, name, created_at) VALUES('" + symbol + "_" + exchange + "', '"+ exchange + "', '"+ name + "', '" + current_time + "');"
        print result
        return result

    def fetch_stock_data(self,key):
        domain_url = "http://m.moneycontrol.com/get_stock_price_mem.php"
        query = "?a=" + key + "&callback=stk_det"
        response = urllib2.urlopen(self.domain_url+query)
        result = self.clean_stock_data(response.read())
        return result

    def close_db_connection(self):
        self.db_connect.close()

    def get_stock_codes(self):
        response = urllib2.urlopen(config['stock_codes'])
        result =  json.loads(response.read())
        stock_file = open("stock_data.json","w")
        with stock_file as outfile:
                json.dump(result, outfile)
        stock_file.close()
        return True

    def load_stock_codes(self):
        stock_file = open("stock_data.json","r")
        result = json.loads(stock_file.read())
        stock_file.close()
        return result

    def stock_data_autotron(self):
        cursor = self.db_cursor
        stock_json = self.load_stock_codes()
        db_logger = open(config['db_logger'],"a")
        pretty_table = PrettyTable(["Stock Code", "Status", "TimeStamp", "Comment"])
        pretty_table.padding_width = 1
        for stock in stock_json:
            stock_data = self.fetch_stock_data(stock_json[stock]['key'])
            for ex in config['exchange']:
                try:
                    query = self.update_query(stock,stock_data[ex],ex)
                    cursor.execute(query)
                    self.db_connect.commit()
                    if(cursor.rowcount == 0):
                        try:
                            insert_query = self.insert_query(stock,ex)
                            cursor.execute(insert_query)
                            self.db_connect.commit()
                            pretty_table.add_row([stock+ "_" + ex ,"SUCCESS" ,time.strftime('%Y-%m-%d %H:%M:%S'),"INSERT"])
                        except Exception,e:
                            self.db_connect.rollback()
                            error =  str(e)
                            pretty_table.add_row([stock + "_" + ex ,"ERROR" ,time.strftime('%Y-%m-%d %H:%M:%S'),error])
                    else:
                        pretty_table.add_row([stock + "_" + ex ,"SUCCESS" ,time.strftime('%Y-%m-%d %H:%M:%S'),"UPDATE"])
                except Exception,e:
                    self.db_connect.rollback()
                    error =  str(e)
                    pretty_table.add_row([stock + "_" + ex ,"ERROR" ,time.strftime('%Y-%m-%d %H:%M:%S'),error])

        self.close_db_connection()
        db_logger.write(str(pretty_table)+"\n")
        db_logger.close()

## For CRON Autotron
StockFiller().stock_data_autotron()

## For Getting New Stock Codes
#StockFiller().get_stock_codes()
